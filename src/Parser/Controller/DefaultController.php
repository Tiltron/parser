<?php

/* 
 * Copyright (C) 2016 Tiltron
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Parser\Controller;

use Silex\Application;
use Parser\Model\Parser;
use Parser\Model\Filter;

/**
 * Class DefaultController
 */
class DefaultController
{
    /**
     * @param Application $app
     *
     * @return mixed
     */
    public function indexAction(Application $app)
    {
        $parser = new Parser();
        $logEntries = $parser->getLogEntries();
        
        if (!$logEntries) {
            return $app->redirect('/error');
        }
        
        $filter = new Filter($app["db.options"]);
        $filter->setCountryCodeFilter(array('DE'));
        
        $logEntriesFiltered = $filter->filterByDB($logEntries);

        /**
         * if no database, check country via api
         * $logEntriesFiltered = $filter->filterByAPI($logEntries);
         */

        return $app['twig']->render('index.html.twig', array(
            'entries' => $logEntriesFiltered,
        ));
    }
    
    /**
     * @param Application $app
     *
     * @return mixed
     */
    public function errorAction(Application $app)
    {
        return $app['twig']->render('error.html.twig', array());
    }    
}