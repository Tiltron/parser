<?php

/* 
 * Copyright (C) 2016 Tiltron
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Parser\Model;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

/**
 * log entry country filter
 * Class Filter
 */
class Filter
{
    /**
     * db settings
     * @var array 
     */
    private $connectionParams;

    /**
     * country code array
     * ('DE', 'US') allows ip's from germany and united states
     * 
     * @var array 
     */
    private $countryCodeFilter = array('DE');

    function getCountryCodeFilter()
    {
        return $this->countryCodeFilter;
    }

    function setCountryCodeFilter($countryCodeFilter)
    {
        $this->countryCodeFilter = $countryCodeFilter;
    }    
    
    public function __construct($connectionParams)
    {
        $this->connectionParams = $connectionParams;
    }

    /**
     * search ip ranges for country code setting
     * 
     * @param array $entries
     * @return array
     */
    public function filterByDB(array $entries)
    {
        $config = new Configuration();
        $conn   = DriverManager::getConnection($this->connectionParams, $config);
        
        $queryBuilder = $conn->createQueryBuilder();
        $queryBuilder->select('i.country')
            ->from('ip_table', 'i')
            ->where(':ip BETWEEN i.ip_long_from AND i.ip_long_to')
            ->andWhere('i.code IN (:codeFilter)');

        $filtered = array();
        foreach ($entries as $entry) {
            $queryBuilder->setParameter(':ip', ip2long($entry->remoteAddr))
                ->setParameter(':codeFilter', $this->getCountryCodeFilter(), \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

            $result = $queryBuilder->execute()->fetchAll();

            if (!empty($result)) {
                $filtered[] = $entry;
            }            
        }
        
        return $filtered; 
    }
    
    /**
     * get ip country code from api
     * 
     * @param array $entries
     * @return array
     */
    public function filterByAPI(array $entries)
    {
        $filtered = array();
        foreach ($entries as $entry) {
            $details = json_decode(file_get_contents('http://ipinfo.io/' . $entry->remoteAddr));

            if (in_array($details->country, $this->getCountryCodeFilter())) {
                $filtered[] = $entry;
            }
        }
        
        return $filtered;
    }
}