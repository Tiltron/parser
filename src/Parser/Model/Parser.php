<?php

/* 
 * Copyright (C) 2016 Tiltron
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Parser\Model;

/**
 * Nginx access.log parser
 * Class Parser
 */
class Parser
{
    /**
     *
     * @var string 
     */
    private $filePath;

    /**
     *
     * @var string 
     */
    private $fileName;

    /**
     * reg ex patterns for log format
     * $remote_addr $server_name $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"
     *
     * @var array 
     */
    private $logFormat = array(
        '(?P<remoteAddr>(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|([0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4}){7})|(([0-9A-Fa-f]{1,4})?(:[0-9A-Fa-f]{1,4}){0,7}:(:[0-9A-Fa-f]{1,4}){1,7}))',
        '(?P<serverName>[a-zA-Z0-9\-\._:]+)',
        '(?P<user>(?:-|[\w-]+))',
        '\[(?P<time>\d{2}/(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)/\d{4}:\d{2}:\d{2}:\d{2} (?:-|\+)\d{4})\]',
        '"(?P<request>(?:(?:[A-Z]+) .+? HTTP/1.(?:0|1))|-|)"',
        '(?P<status>\d{3}|-)',
        '(?P<sentBytes>[0-9]+)',
        '"(?P<referer>.*?)"',
        '\"(?P<userAgent>.*?)"',
    );
    
    /**
     * combined regEx pattern
     * @var string 
     */
    private $pattern;    
    
    function getFilePath()
    {
        return $this->filePath;
    }

    function getFileName()
    {
        return $this->fileName;
    }

    function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function __construct()
    {
        $this->setFilePath(FILEPATH);
        $this->setFileName(FILENAME);
        
        $this->pattern = '#^' . implode(' ', $this->logFormat) . '$#';
    }
    
    /**
     * get log content and start parsing by line
     * @return array|false
     */
    public function getLogEntries()
    {
        $logContent = $this->getLogContent();
        
        if (!$logContent || !is_array($logContent)) {
            return false;
        }
        
        $logEntries = array();
        foreach ($logContent as $entry) {
            $logEntries[] = $this->parse($entry);
        }

        return $logEntries;
    }

    /**
     * read entire file into an array
     * @return array|false
     */
    private function getLogContent()
    {
       $data = file($this->getLogFilePath(), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

       return $data;
    }

    /**
     * @return string
     */
    private function getLogFilePath()
    {
        return $this->getFilePath() . $this->fileName;
    }

    /**
     * parse log entry via regEx with combined format pattern
     *
     * @param string $entry
     * @return object \stdClass
     * @throws Exception
     */
    public function parse($entry)
    {
        if (!preg_match($this->pattern, $entry, $result)) {
            throw new \Exception($entry);
        }
        
        $logEntry = new \stdClass();
        foreach (array_filter(array_keys($result), 'is_string') as $key) {
            if ('time' === $key && true !== $stamp = strtotime($result[$key])) {
                $logEntry->stamp = $stamp;
            }
            $logEntry->{$key} = $result[$key];
        }
        
        return $logEntry;
    }
}